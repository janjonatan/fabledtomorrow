# Game Design Document

- [Game Design Document](#game-design-document)
  - [Projektin tiedot](#projektin-tiedot)
  - [Johdanto](#johdanto)
    - [Pelimekaniikat lyhesti](#pelimekaniikat-lyhesti)
    - [Suunitellut alustat](#suunitellut-alustat)
    - [Monetisaatio](#monetisaatio)
  - [Projektin laatu, luonne ja laajuus](#projektin-laatu-luonne-ja-laajuus)
    - [Projektin oletettu kesto](#projektin-oletettu-kesto)
    - [Projektin tiimin koko](#projektin-tiimin-koko)
  - [Vaikutteet lyhyesti](#vaikutteet-lyhyesti)
    - [Shin Megami Tensei-pelisarja](#shin-megami-tensei-pelisarja)
    - [Escape From Tarkov](#escape-from-tarkov)
    - [Undertale](#undertale)
    - [Fable](#fable)
  - [Hissipuhe](#hissipuhe)
  - [Lyhyt kuvaus projektista](#lyhyt-kuvaus-projektista)
  - [Pidempi kuvaus projektista](#pidempi-kuvaus-projektista)
  - [Mikä erottaa tämän pelin muista](#mik%c3%a4-erottaa-t%c3%a4m%c3%a4n-pelin-muista)
  - [Keskeisimmät pelimekaniikat pidemmin](#keskeisimm%c3%a4t-pelimekaniikat-pidemmin)
  - [Varusteiden päivitys](#varusteiden-p%c3%a4ivitys)
  - [Entiteetti mekaniikka](#entiteetti-mekaniikka)
  - [Taistelumekaniikka](#taistelumekaniikka)
  - [Lista entiteeteistä](#lista-entiteeteist%c3%a4)
    - [Vaena](#vaena)
    - [Bertha](#bertha)
  - [Tarina ja maailma](#tarina-ja-maailma)
    - [Tarina lyhyesti](#tarina-lyhyesti)
    - [Maailma kuvailtuna lyhyesti](#maailma-kuvailtuna-lyhyesti)
  - [Maailma kuvailtuna pidemmin](#maailma-kuvailtuna-pidemmin)
    - [Historia ja tausta](#historia-ja-tausta)
    - [Entiteetit](#entiteetit)
  - [Tarvittavat assetit](#tarvittavat-assetit)
    - [2D grafiikat ja animaatiot](#2d-grafiikat-ja-animaatiot)
    - [3D grafiikat ja animaatiot](#3d-grafiikat-ja-animaatiot)
  - [Äänet ja musiikit](#%c3%84%c3%a4net-ja-musiikit)
  - [Koodi](#koodi)

## Projektin tiedot

\<The Fabled Tomorrow>

Tekijät: Ari-Pekka Savolainen, Aleksi Rajamäki, Ervo Halonen, Ilkka Säkkinen, Jan-Jonatan Linna, Laura Lindeman, Mikael Salmela

## Johdanto

Peli sijoittuu post-apocalyptiseen maailmaan jossa demonit, jumalat ja
muut yliluonnolliset entiteetit ovat arkipäivää siinä missä digitaaliset
virtuaalilemmikit ovat totta meille. Maailmanloppu ei tullut nopean
ydinsodan, ilmastokriisin tai muunkaan pikaisen tapahtuman kautta, vaan
maailma on hitaasti rappeutunut tilaan missä se on. Ihmiset taistelevat
niistä pienistä resursseista, joita maailmasta enää löytyy. Jotkut ovat
liittoutuneet auttaakseen toisiaan, jotkut ovat liittoutuneet ottaakseen
toisilta ja jotkut ovat niitä, jotka eivät kuulu mihinkään liittoumaan tai
ryhmään, vaan pitävät huolta itsestään. Pelin alussa pelaaja kuuluu tähän
jälkimmäiseen ryhmään ja pelin edetessä pelaaja itse valitsee, haluaako
kuulua mihinkään ryhmään.

### Pelimekaniikat lyhesti

- Vuoropohjainen rpg
- Etä ja lähitaistelu aseet, spellit ja summonit sekä kertakäyttö itemit
- Tarina vaikuttaa tulevaan, ei suoraan moraalivalintoja
- 3D-peli mutta liikutaan käytännössä vain XY-akselilla

### Suunitellut alustat

- Pc

### Monetisaatio

- Kerta ostos, alustana PC:llä Steam sekä ichi.io
- Monetesaatio katsotaan projektin varmistuessa

## Projektin laatu, luonne ja laajuus

### Projektin oletettu kesto

Oletettavissa että kuuden henkilön ryhmällä pelin tekeminen vie noin
puolivuotta. Grafiikat ovat hyvin yksinkertaiset low-poly henkiset, mutta
ryhmän pitää löytää yhteinen sävel jonka löytäminen voi kestää
muutamankin viikon.

### Projektin tiimin koko

- Graafikot
  - Graafinen ohjaaja
  - UI-graafikko
  - Hahmot (pelaaja, npc, demonit) ja animointi
  - Assetit ja ympäristö
  - Sketsaaja
  - 2D sprite taisteluihin
- Ääni
  - Ääniohjaaja
    - Musiikki
    - Äänimaailma ja ääniefektit
- Koodaus
  - Liikkuvuus ja maailma
  - Taistelut
  - Core mekaniikan ulkopuolinen koodaus
- Käsikirjoitus ja maailma
  - Ohjaaja
  - Tuottaja / Scrum Master
  - Hahmot ja maailma
  - Dialogit
- Muut
  - Taistluiden vastaava
  - Pelin progressin ja taistelujen balansoija
  - QA-vastaava
  - QA

## Vaikutteet lyhyesti

### Shin Megami Tensei-pelisarja

Pelimekaanikan yksi vahvimmista vaikuttajista on Shin Megami Tenseipelisarja (jatkossa SMT). SMT:stä on saatu vaikutteita kuinka mytologiat
voidaan implementoida nykyaikaiseen pelimaailmaan lähes saumattomalla
tavalla. Demonit ja jumalat menettävät mahtipontisuutensa ja
kaikkivoipaisuutensa mutta ovat kuitenkin hyvin vahvoja yksilöitä jotka nousevat normaalien ihmisten yläpuolelle. Myös vuoropohjaisentaistelun
core tulee SMT:stä.

### Escape From Tarkov

EFT on hyvin realistinen taistelusimulaatiopeli, jonka parhaimpiin
ominaisuuksiin kuuluu varusteiden muokkaus ja kasaus. Haluan ottaa tästä
pelistä juuri tämän ominaisuuden RPG:hen ja korvata sillä vanhan ja tutun
EXP-progressi mekaniikan. Hahmot eivät saa leveleitä tai kokemusta pelissä,
vaan progressi tapahtuu varusteita keräämällä, päivittämällä ja
muokkaamalla.

### Undertale

Undertalen lyhyt mutta ytimekäs ja rikas maailma inspiroi kirjoittamaan
hahmorikkaan maailman jossa pelimekaniikka on yksinkertainen hahmottaa,
mutta monipuolinen viihdyttämään tarpeeksi pitkään että tarina saadaan
kasaan kunnolla. Undertale myös antaa inspiraatiota kuinka jokainen yksilö
on uniikki (myös viholliset) ja luonteiden ja vahvuuksien korostaminen
jokaisessa on tärkeää hahmon ja maailman syvyyden kannalta.

### Fable

Fable oli minulle ensimmäinen peli joissa moraalivalinnoille on selvä
vaikutus pelissä. Pelin ympäristö (hahmot ja muut dynaamiset mekaniikat)
muuttuu pelaajan valintojen perusteella. En halua ottaa suoranaista
moraalivalintoja mukaan, vaan kuinka tietyt teot vaikuttavat ympäristöön
joka muuttaa asioita joihin pelaaja voi olla interaktiossa.

## Hissipuhe

“Kuvittele että japanilaiset roolipelit saavat lapsen länsimaalaisten
asekeskisten varustelupelien kanssa. Lisää siihen lyhyt mutta rikas kokemus
ja maailma joka muuttuu pelaajan ja sen valintojen mukana. Tämä kaikki
kuoritaan karuun mutta kauniiseen simplistiseen lowpoly kuoreen ja
tarjotaan pelaajalle kuuden tunnin kokemuksen muodossa!”

## Lyhyt kuvaus projektista

Vuoropohjainen roolipeli joka sijoittuu post-apokalyptiseen, ”tylsään” ja
tyhjään maailmaan jossa resurssit ovat lopussa. Pelin progressi tapahtuu
hahmon varusteiden parantumisen myötä eikä normaalin roolipelimäisen
levutuksen kautta. Pelissä hyödynnetään eri uskomusten, kansantarujen ja
legendojen henkilöitä ja entiteettejä demonien ja jumalten muodossa (niitä
ei tod.näk. kutsuta demoneiksi ja jumaliksi). Osa entiteeteistä on tarpeeksi
vahvoja aktualisoitumaan fyysiseen muotoon ns. summoneiden muodossa
(Final Fantasy 7 / 8) ja jotkut voivat antaa pelaajalle voimia ns. taikojen
muodossa. Pelissä tehtävät valinnat (tai tekemättä jäänet valinnat)
vaikuttavat ympäristöön ja hahmoihin, mutta ei niin ratkaisevasti että
monimutkaista ”branchaystä” tarvitsee muodostaa maailman sisälle. Pelissä
on kolme loppua, jotka menevät tutuilla termeillä, ”good”, ”neutral” ja
”bad” ending. Nimet eivät ole lopullisia.

## Pidempi kuvaus projektista

Taisteluissa pelaajalla on valittavinaan lähitaistelu (melee), pitkänmatkan
taistelu (ampuminen), spellit (ominaisuudet joita entiteetit antavat
pelaajalle), summonointi (entiteettinen materialisointi vuoron ajaksi
maailmaan / taisteluun), kertakäyttöesineet (itemit) ja puolustus (nostaa
mahdollisuutta väistää hyökkäykset seuraavaan vuoroon saakka). Taisteluista
ei saa kokemusta (exp) eikä tätä myöten leveitä. Hahmo itsessään ei parane,
mutta pelaaja voi löytää, saada ja rakentaa itselleen uusia osia aseeseen
joilla voi parantaa damage inputtia. Varusteet vaikuttavat puolustusarvoon
sekä muihin statseihin, kuten ammusten määrään, kuinka paljon tavaraa voi
löytyä, entiteettien damageen jne, hyvin perus roolipeli kamaa. Osa
entiteeteistä tulee automaattisesti joka kerta pelin myötä, osa pitää löytää
tutkimalla maailmaa ja osa tulee tekemällä tiettyjä tehtäviä. Entiteetit
opitaan lukemalla tiedosto pelaajan digilaitteesta (vähän kuten pipboy tai
pokedex) ja tiedostoja löytää tosiaan pelin edetessä. Ns. hyvää reittiä
edetessä pelaaja saa enemmän entiteettejä jotka antavat pelaajalle
erilaisia buffeja (bonuksia jotka parantavat joitain kykyjä) sekä spellejä
joilla voidaan parantaa pelaaja. Neutraalissa reitissä taas tulee vaihtelevasti
passiivisia, aktiivisia, agressiivia ja defensiivisiä kykyjä ja taas ns. pahaa
reittiä edetessä pelaajalle annetaan hyvin paljon vahinkopohjaisia
entiteettejä.

Peli tapahtuu kolmen pelipäivän aikana, jonka aikana on kolme
”päätehtävää” jotka määrittelevät lopun ja kuinka peli etenee seuraavana
päivänä. Tehtävät ovat moniulotteisia, joihin voi lähestyä aina kolmella
tavalla, auttamalla jompaakumpaa osapuolta (joko ns. hyvin tai pahis) joka
sitten antaa ”moraalipisteitä” loppua varten, tehtäviä voi jättää myös
tekemällä jolloin ei tulee ”moraalipisteitä” lainkaan.
Pelin loput on seuraavat. ”Hyvässä” lopussa pelaaja saapuu päämajaansa,
kotiinsa, tukikohtaansa jossa hän poistaa varusteet joita hän ei ole poistanut
koko pelin aikana (suojapuku, kaasunaamari, aseet), menee ovesta sisään ja
maailma transioituu normaaliin nykyhetkeen. Hahmo sanoo jotakuinkin
”Maybe there is still hope in this world”, ja peli päättyy. Neutraalissa
lopussa hahmo menee nukkumaan, herää seuraavaan päivään ja peli jatkuisi
ns. normaalisti, mutta peli vain päättyy. Pahassa lopussa pelaajahahmo on
täysin hyväksynyt maailman tilanteen ja käyttää hyväkseen muiden
heikkoutta ja omia vahvuuksiaan vahvistaakseen omaa asemaansa. Lopussa
näkyy kun pelaaja seuraavana päivänä aamuna herätessään päättää
potkaista ”naapurin” oven sisään ja teurastaa naapurin sinne. Tätä
implikoidaan aseen äänellä, mutta ruutu menee mustaksi ennen kuin mitään
graafista kerkeää näkyä.

Pelin grafiikat ovat hyvin simplististä, PS1 / PS2 tason lowpoly meininkiä.
Maailma on kolmiulotteinen mutta liikkuminen käytännössä tapahtuu
kuitenkin vain kaksiulotteisella tasolla. Kamera on kolmannesta persoonasta
ja seuraajaa pelaajaa joka paikkaan. Tietyissä huoneissa / tiloissa kameran
sijainti vaihtuu tunnelman luomisen takia tai jos jotain tiettyä elementtiä
halutaan korostaa. Pelissä ei ole ääninäyttelyä, mutta tekstien tulessa
ruudulle puhe on korvattu jollain äänellä (kuten japanilaisissa roolipeleissä
on tapana kun tekstiä tulee ruudulle).

## Mikä erottaa tämän pelin muista

- Monipuolinen mytologioiden hyödyntäminen roolipelissä
- Länsimaalainen ”aseporno” kohtaa vuoropohjaisen roolipelin
- Lowpoly post-apokalyptinen graafinen ulkoasu
- Lyhyt ja immersiivinen roolipelikokemus
- Roolipelimekaniikka poikkeaa vahvasti standardista

## Keskeisimmät pelimekaniikat pidemmin

Vuoropohjainen taistelu. Peli toteutetaan joko dungeon crawl tyylisellä
simppelillä taisteluasetelmalla (kts. kuvat 1. ja 2. alapuolella) tai sitten
tutumalla tausta / kenttä riippuvaisella 3D-generoidulla taisteluilla (kuvat 3.
ja 4.)

xxx.

Teorissa sillä ei ole merkitystä kumpi taistelumekaanikoista on käytössä,
sillä taistelun core on edelleen identtinen, vain visuaalinen tyyli muuttuu.
Ensimmäisen personaan taistelu on hyvin vanhentunut tyyli hoitaa taistelut,
mutta sillekin tyylille on oma kannattajakuntansa. Projektin päätyttyä olisi
mahdollista jälkikäteen tuoda päivitys jossa voi vaihdella 2D- ja 3Dtaistelujen väliltä

Vaikka pelissä ei ole normaaleita tasoja hahmolla, kuitenkin erilaiset statsit
vaikuttaa taistelussa vahvasti, jotka ovat jotakuinkin seuraavat.

- Weapon damage (”Wpn dmg”,tuliaseen vahinko)
- Rate of fire (”RoF”, basic shot toiminnon laukausten määrä)
- Accuracy (”Acc”, Osuma- ja kriittisenvahingon todennäköisyys)
- Magazine size (”Mag”, Lippaan koko, yksi vuoro käytetään lippaan
vaihtamiseen)
- Melee damage (”Mel dmg”, lähitaisteluaseen vahinko)
Melee hits (”Mel hit”, lähitaisteluaseen iskujen määrä yhdellä
lyönnillä)
- Speed (”Spd”, Taistelun vuorot määräytyy speedin mukaan, vaikuttaa
myös väistämiseen
- Armour (”Arm”, vaikuttaa vahingon määrään, suojavarusteet voivat
riikkontua)
- Entity control (”Ent ctr”, vaikuttaa kuinka monta entiteettiä voi
kerralla olla ”mukana”)
- Entinty power (”Ent pwr”, vaikuttaa entiteettien vahinkoon ja muihin
ominaisuuksiin, kuten buffien keston määrään)

## Varusteiden päivitys

Koska pelissä ei ole kokemuspisteitä, eikä tasoja pitää hahmon kuitenkin
parantua jotenkin, jotta peliin saadaan progressia ja progressin tuntua.
Tasot korvataan varusteilla ja niiden päivityksellä, kuten peleissä kuten Rise
of the Tomb Raider tai Escape From Tarkov. Ase pysyy samana koko pelin,
mutta aseen ominaisuuksia ja osia voi parannella mielensä mukaan jotta
pelaaja saa haluamansa kokonaisuuden, voi keskittyä suoraan vahinkoon,
sarjatulen nopeuteen, panosten kokomaismäärään, osumatarkkuuteen jne.
Tämä on aivan pelaajan päätettävissä. Kuva1. näyttää simplistisemmän
tavan päivitellä aseita ja kuva 2. taas on monimutkaisempi ja syvällisempi.
Näistä pitää löytää se oikea tasapaino.

xx

Aseen päivittäminen vaihtaa myös aseen ominaisuuksia, esimerkiksi
etukahvan asentaminen nostaa kertatulen RoF statsia, punapistetähtäin
nostaa Acc ja RoF statsia, parempi perä nostaa Acc statsia jne.
Aseessa on kustomoitavissa seuraavat osat (lueteltuna aseen piipusta perään
järjestyksessä).

- Liekinvaimennin
  - Liekinvaimennin
  - Äänenvaimennin
- Etutähtäin
  - Jos ei ole muuta tähtäintä, mikäli on niin tämän antamat
vaikutukset häviävät
- Piippu
- Etupää
  - Alakisko
    - Etukahva
    - Kranaatinheitin
  - Sivukisko
    - Lamppu
    - Lasertähtäin
- Runko
- Tähtäinkisko
- Lipas
- Kahva
- Perä

## Entiteetti mekaniikka

Entiteetit, kuten jumalat ja demonit toimivat pelissä spelleinä ja joistain
peleistä tuttuina summoneina. Entiteetit joko antavat pelaajalle voimia tai
jos entiteetit ovat itsessään tarpeeksi vahvoja voivat he materialisoitua ja
ottaa fyysisen muodon. Tällöin ne ovat x-vuoron pelissä ohjattavina
lisähahmoina (kuva 1) tai he antavat vaan roolipeleistä tuttuja spellejä.
Mitään taikapisteitä tai manaa ei ole, vaan riippuen entiteetistä voi yhdessä
taistelussa käyttää entiteettien voimia x-kertaa.

Entiteettejä voi olla Ent ctr statsin mukainen määrä. Entiteettejä voi
vaihtaa taistelun ulkopuolella toisiin, mutta ei taistelun aikana. Jotkin
entiteetit voivat antaa myös passiivisia bonuksia, joten tästä syystä niitä ei
voi vaihtaa kesken taistelun. Entiteetit jotka ovat summonoitavia, eivät voi
antaa passiivisia bonuksia koska ne voivat ”kuolla” kesken taistelun.
Entiteettien erilaiset ominaisuudet ovat jotakuinkin seuraavanlaisia

- Agressiiviset kyvyt
  - Melee hyökkäyksiksi laskettavat kyvyt
  - Ranged hyökkyksiksi laskettavat kyvyt
  - Uniikit entiteettihyökkäykset johon puolustus tai muut statsit
eivät vaiktua
- Defensiiviset kyvyt
  - Parannus (parantaa pelaajan elämäpisteitä)
  - Buffit (nostaa eri statseja, kuten Acc tai Spd taistelun tai xmäärän kierroksia ajaksi).
  - Debuffit (laskee vastustajien statseja, kuten buffit mutta
käänteisenä)
- Passiiviset kyvyt (nostaa pelaajan statseja kunnes entiteetti
poistetaan käytöstä)
- Summon kyky
  - Summonoidulla entiteetillä on omat statsit
    - Kesto
    - Speed
    - Damage (vaikuttaa kokonaisvaltaisesti kaikkiin kykyihin)
    - Accuracy
    - Summonoitu entiteetti toimii ns. toisena hahmona taistelussa
kunnes se kuolee tai kutsutaan pois.
  - Entiteetillä on omat kyvyt, yksi tai useampi riippuen
entiteetistä.
  - Kun entiteetti on kentällä, vastustajilla on 50% mahdollisuus
hyökätä entiteettiin, ellei sillä ole jotain kykyä joka laskee tai
nostaa tätä mahdollisuutta

## Taistelumekaniikka

Taistelussa pelaajalla on käytössä seuraavat valinnat

- Ranged weapon
  - Basic shot (laukausten määrä määrittyy ROF mukaan)
  - Burst (ROF x 2 laukausta, ACC 80% ensimmäisellä ja 50% toisella
pyrskeellä)
  - Snipe (Jos tähtäin tai muu tekijä antaa tämän kyvyn. Ampuu
vain yhden laukauksen, nostaa pelaajan ACC 100% (vastustajan
SPD vaikuttaa silti) ja tekee kriittisen osuman, jos se on
mahdollista.
  - Special (Entiteettien tai varusteiden antama lisäominaisuus)
- Melee weapon (Lyöntien määrä määrittyy MEL HIT mukaan)
- Entity skill (Entiteettien antamat taidot, taitojen määrittyy ENT CTR
mukaan)
- Entity summon (Kutsuu entiteetin taisteluun mukaan, vain yksi
entiteetti voi olla kerrallaan mukana. Entiteetillä on omat statsit
jokaisen entiteetin voi kutsua vain kerran per taistelu)
- Item (Kertakäyttöisten tavaroiden käyttäminen)
- Defence (puolustautuu, SPD nousee yhden kieroksen ajan radikaalisti)

Taistelun ja jokaisen vuoron aluksi määritetään kierroksen
hyökkäysjärjestys. Ensin verrataan SPD statseja, sitten ACC ja lopuksi HP
statsia. Mikäli tämänkin jälkeen tilanne on tasan, aloittaa vihollinen. Mikäli
vihollisia on useampia, arvotaan näidenkin järjestys. Jos summonoidun
entiteetin statsit ovat tasan pelaajan kanssa, näiden järjestys arvotaan.
Ohessa esimerkki koodi. Jos muuttujan edessä on p, pelaajan statsia ja v,
vihollisen statsia. Aloittaja = 1 tarkoittaa että pelaaja aloittaa ja Aloittaja =
2 tarkoittaa että vihollinen

xx

Osuminen lasketaan seuraavalla kaavalla. Jos muuttujan edessä on p,
pelaajan statsia ja v, vihollisen statsia. Tämän jälkeen arvotaan luku 1 ja
100 väliltä ja katsotaan onko arvotta luku pienempi kuin osuminen, mikäli
on niin laukaus osui. Mikäli ROF on suurempi kuin 1, niin jokaiselle
ammukselle tehdään erikseen tämä sama testi. Mikäli panokset loppuu
kesken sarjatulen tai useamman laukauksen, niin laukauksia ei tapahdu.

x

Tässä kaavassa huomioidaan vastustajan ARM statsi, mitä isompi se on, sitä
suuremmaksi voidaan suojauksen olevan joka taas hidastaa väistämistä.
Tämä tasen kompensoidaan sillä, että vahinkoa otetaan vähemmän. Tällä
periaattella ARM statsin nostaminen on hyödyllisempää kuin SPD statsin
nostaminen vahingon minimoimiseksi.

## Lista entiteeteistä

### Vaena

Vaena, eli Väinämöinen, on Kalevalan mytologian yksi keskeisimmistä
henkilöistä. Vaena on saanut vahvasti vaikutteita Akseli Gallen-Kallelan
kuvasta Sammon puolustus (Kuva 1), johon Vaenan design sekä tekninen
toteutus pohjautuu (Kuva 2 ja Kuva 3). Vaenalla on aseenaan miekka kuten
maalauksessa Väinämöisellä on ja hänen päänsä vastaa hauen kalloa,
vastaten tarua jossa Väinämöisellä on hauanluusta tehty kannel. Vaena on
summontoitava defensiivinen entiteetti, jonka passiivisena kykynä on ottaa
vastaan osumia itseensä, ns. tankata vastustajia. Vaena on ensimmäinen
entiteetti joka pelaajalla on ja se on vakiona alussa mukana.

xx

### Bertha

Bertha, eli Perchta tai Percht, on vanhasta Alppi pakanauskonnoista lähtöisin
oleva hahmo. Perchta oletettavasti tarkoittaa ”The Bright One”. Perchtaa
usein kuvaillaan yksi jalkaiseksi olennoksi, jonka jalka muistuttaa hanhen tai
joutsenen räpylää. Tämä yksijalkaisuus symbolisoi hänen mahtavuuttaa ja
sitä, kuinka hän pystyy muuttaa muotoaan eläimeksi. Tarinoissa Pertcha
tietää onko lapset olleet kilttejä vai ei, ja ei, niin hän viiltää lapsien vatsat
auki, poistaa vatsalaukun ja suolet ja tämän jälkeen täyttää vatsan heinällä
ja pienillä kivillä. Hänen legendassaan on useita muitakin puolia, mutta
tässä designissa on keskityttä vain ylläoleviin kuvailuihin. Kuva 1 on ollut
inspiraation lähteenä ja Kuva 2 on luonnos hahmosta. Kuvauksissa ei oltu
kerrottu millä hän viiltää vatsat auki, joten lisäsimme hänelle kolme sormea
jotka kaikki ovat pitkiä, veitsen omaisia. Hänen käsistään myös roikkuu
suolia.

xx

## Tarina ja maailma

### Tarina lyhyesti

Kts. Theme / Setting / Genre

### Maailma kuvailtuna lyhyesti

Post-apokalyptinen maailma joka on suistunut siihen tilaan missä se on
aikojen saatossa lähes itsekseen. Useat sodat on sodittu ja luonnon
katastrofit on kärsitty. Suurhallinnot ovat lopettaneet toimintansa
infrastruktuurien murennuttua aikojen saatossa. Sähkö on yleisyys joihin
kaikilla kaupungeilla ei ole varaa. Pelaaja elää oma elämää ja etsii
paikkaansa yhteiskunnassa jossa on normaalia elää entiteettien, jumalten ja
demonien kaltaisten olentojen kanssa.

## Maailma kuvailtuna pidemmin

### Historia ja tausta

Eletään vuotta 2xxx (ei tarkemmin väliä, plus tämä referenssi viihdyttää
retropelaajia) jolloin meille tuttu maailma on rappeutunut ja kuihtunut
olemattomiin, tilalle on tullut kylmä, harmaa ja rankka maailma jossa
selviää joko luonnottomalla yksilövoimalla tai yhteistyöllä (myös pelin
teema, selvittää haluaako pelaaja olla yksin voimakas vai toimia osana
kokonaisuutta). Useat maailmansodat ja luonnon katastrofit ovat saattaneet
maailman siihen tilaan missä se tällä hetkellä on. Sodat tappoivat miljoonia
ja miljoonia, luonnon katastrofit ovat muuttaneet maailmaa paikoittain
asumiskelvottomaksi. Auringon aktiivisuuden nouseminen on johtanut suurin
auringonpurkauksiin ja ennen taivasta täyttänyt satelliittipeite on poistunut
aikoja sitten. Infrastruktuuri jolla ylläpidettiin voimaverkostoja on pettänyt,
virtaa ei tule kuin harvoihin alueisiin ja sinnekin yleensä vain heikosti.
Suurinosa maailman ydinvoimailoista on rappeutunut ja tehnyt alueet lähellä
ja kaukaa asumiskelvottomiksi, huhuja kuitenkin on että näilläkin alueilla
asuu vielä ihmisiä. Kaikki tämä tapahtui useita sukupolvia sitten eikä näistä
tapahtumista enää oikeastaan enää välitetä, tilanne on mitä on. Valtioita ei
enää ole, paikallishallintojakaan ei enää suoranaisesti ole vaan asutusalueita
hallitsee yhteisöt, liigat tai muut pienhallinnot. Pelin tapahtumat sijoittuvat
nimettömän suurkaupungin läheiseen teollisuuskaupungin laitamille, joka on
rakentunut robotiikkatehtaiden ympärille. Alue jakautuu kolmeen osaan,
tehdasalueeseen, tiheään keskikaupunkialueeseen ja kaupunki laitamaalueeseen.

### Entiteetit

Entiteetit ovat olleet osana tätä fiktiivistä yhteiskuntaa kirjoitetun historian
alkuhetkistä saakka. Näitä entiteettejä on kutsuttu aikojen saatossa mm.
jumaliksi, demoneiksi, enkeleiksi ja muiksi yliluonnollisiksi hengiksi.
Vieläkään ei tiedetä mistä ne ovat tulleet ja miksi uusia entiteettejä ei
ilmesty. Toisen vuosituhanneen vaihteessa kehitelty teknologia mahdollisti
entiteettien digitalisoimisen ylös kaivetuista artifakteista, haudoista,
kääröistä ja muista esineistä, jotka oli tehty näiden palvomista tai
uhraamista varten. Ei mennyt kauaa kuin tämän teknologian myötä saatiin
aikaan teknologia joka mahdollisti entiteettien materialisoitumisen fyysisiksi
olennoiksi. Jotkut entiteeteistä olivat niin vahvoja, että ne saivat oman
kehon, oman muodon. Jotkut taas eivät ole tarpeeksi vahvoja
ilmeentymään, vaan käyttävät toisten kehoja hyödykseen ja tulevat osaksi
heitä. Entiteettejä on myös mahdollista sijoittaa ns. nukkeihin joissa he
voivat vapaasti liikkua kunnes heidän voimansa ehtyvät ja he joutuvat
vaipumaan horrokseen. Mikäli heidän keinotekoinen kehonsa rikkoontuu,
pitää heidät asettaa uuteen kehoon ja toivoa että keho on yhteensopiva
entiteetin kanssa. Ajan saatossa teknologia joka mahdollisti entiteettien
kutsumisen on kuitenkin lähes kadonnut ja nyt vain murto-osalla ihmisistä on
enää mahdollisuus olla ns. Entity Wieldereitä (entiteettien kutsujia).

## Tarvittavat assetit

### 2D grafiikat ja animaatiot

- Taustat taisteluille
  - Riippuen paikasta jossa taistellaan, taistelun tausta vaihtuu

- Vihollisten spritet
  - Vihollisten animaatiot
- Efektit taistelussa (voi olla myös 3D)
- Valikot
- UI
- Ikonit
- Fontit
  
### 3D grafiikat ja animaatiot

- Hahmot
  - Modellit
  - Viholliset
  - Animointi.
    - Liikkuminen
    - Paikallaanolo
    - Puhuminen
    - Taisteluanimaatiot
      - Lyönti
      - Taiat
      - Summonit
      - Ampuminen
      - Vahingoittuminen
      - Kuoleminen
- Assetit
- Rakennukset ja ympäristöt- Animointi

## Äänet ja musiikit

- Musiikit
  - Eri ympäristöjen musiikit
  - Taistelumusiikkit
        - Perus taistelumusiikki
        -  Variaatiot taistelumusiikkeihin
        - Bossimusiikki
- Ambientit
- Efektit
  - Tekstin vieritys
  - Valikot
  - Ampuminen
  - Lyöminen
  - Spellit
  - Summonit
  - Kävely ja juoksu

## Koodi

- Liikuteltavuus
- Taistelu
  - Kuinka statsit vaikuttaa jne
- Pelin eventit
- Kuinka valinnat vaikuttaa jne
